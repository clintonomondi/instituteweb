@extends('layouts.app')

@section('content')
    <div class="bg-image page-title">
        <div class="container-fluid">
            <a href="#"><h1>Diploma in Logistics and Transport</h1></a>
            <div class="pull-right">
                <a href="01_home.html"><i class="fa fa-home fa-lg"></i></a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href="06_services.html">Our services</a>&nbsp;&nbsp;|&nbsp;&nbsp; <a href="08_services-details.html">Road Transportation</a>
            </div>
        </div>
    </div>

    <div class="container-fluid block-content">
        <div class="row main-grid">
            <div class="col-sm-9 posts">
                <div class="big-posts">
                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <img src="assets/media/blog/5.jpg" alt="Img">
                        <div class="post-info">
                            <span>BY JOHN DEO</span>
                            <span>JUN 29, 2015</span>
                            <span>Delivery, Cargo, Shipment</span>
                            <span>3 Comment(s)</span>
                        </div>
                        <h1>Units offered </h1>
                        <div class="post-content">
                      <table class="table table-striped table-bordered">
                          <tr>
                              <th>#</th>
                              <th>#</th>
                          </tr>
                          <tbody>
                          <tr>
                              <td>Unit 1</td>
                              <td>Management of Logistics and transport environment</td>
                          </tr>
                          <tr>
                              <td>Unit 2</td>
                              <td>Customer care and service quality</td>
                          </tr>
                          </tbody>
                      </table>
                        </div>
                        <a href="11_blog-details.html" class="btn btn-success btn-default read-more">READ MORE</a>
                    </div>
                </div>
                <nav class="pagination wow fadeInUp" data-wow-delay="0.3s">
                    <a href="#">1</a>
                    <a href="#" class="active">2</a>
                    <a href="#">3</a>
                    <a href="#">4</a>
                    <a href="#">5</a>
                </nav>
            </div>
            <div class="col-sm-3">
                <div class="sidebar-container">
                    <div class="wow slideInUp" data-wow-delay="0.3s">
                        <form class="search-form" action="#" method="post">
                            <input type="text" placeholder="Search Blog" name="query">
                            <input type="submit" name="Search" value="Search" class="hidden">
                            <i class="fa fa-search"></i>
                        </form>
                    </div>
                    <div class="wow slideInUp" data-wow-delay="0.3s">
                        <h4>Categories</h4>
                        <ul class="blog-cats">
                            <li><a href="10_blog.html">Sea Freight</a></li>
                            <li><a href="10_blog.html">Door-to-Door Delivery</a></li>
                            <li><a href="10_blog.html">Latest Shipments</a></li>
                            <li><a href="10_blog.html">Road Ways Cargo</a></li>
                            <li><a href="10_blog.html">Submenu</a></li>
                            <li><a href="10_blog.html">Railway Logistics</a></li>
                            <li><a href="10_blog.html">Cargo Services</a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
