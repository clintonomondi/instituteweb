@extends('layouts.app')

@section('content')

    <div id="owl-main-slider" class="owl-carousel enable-owl-carousel" data-single-item="true" data-pagination="false" data-auto-play="true" data-main-slider="true" data-stop-on-hover="true">
        <div class="item">
            <img src="assets/media/main-slider/1.jpg" alt="slider">
            <div class="container-fluid">
                <div class="slider-content col-md-6 col-lg-6">
                    <div style="display:table;">
                        <div style="display:table-cell; width:100px; vertical-align:top;">
                            <a class="prev"><i class="fa fa-angle-left"></i></a>
                            <a class="next"><i class="fa fa-angle-right"></i></a>
                        </div>
                        <div style="display:table-cell;">
                            <h1>THE INSTITUTE OF LOGISTICS AND TRANSPORT</h1>
                        </div>
                    </div>
                    <p>Nunc accumsan metus quis metus. Sed luctus. Mauris eu enim quisque dignissim nequesudm consectetuer dapibus wn eu leo integer varius erat.<br><a class="btn btn-success" href="#">ABOUT US</a></p>
                </div>
            </div>
        </div>
        <div class="item">
            <img src="assets/media/main-slider/2.jpg" alt="slider">
            <div class="container-fluid">
                <div class="slider-content col-md-6 col-lg-6">
                    <div style="display:table;">
                        <div style="display:table-cell; width:100px; vertical-align:top;">
                            <a class="prev"><i class="fa fa-angle-left"></i></a>
                            <a class="next"><i class="fa fa-angle-right"></i></a>
                        </div>
                        <div style="display:table-cell;">
                            <h1>THE OPPORTUNITY TO MAKE DIFFERENCE HAS NEVER BEEN GREATER</h1>
                        </div>
                    </div>
                    <p>Nunc accumsan metus quis metus. Sed luctus. Mauris eu enim quisque dignissim nequesudm consectetuer dapibus wn eu leo integer varius erat.<br><a class="btn btn-success" href="#">LEARN MORE</a></p>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row column-info block-content">
            <div class="col-sm-4 col-md-4 col-lg-4 wow fadeInLeft" data-wow-delay="3.3s">
                <img src="assets/media/3-column-info/1.jpg" alt="slider">
                <span></span>
                <h3>SAFE & SECURE DELIVERY</h3>
                <p>Integer congue, elit non semper laoreet sed lectus orci posuh nisl tempor lacus felis ac mauris. Pellentesque in urna. Intege vitae felis vel magna posuere vestibulum.</p>
                <a class="btn btn-default btn-sm" href="11_blog-details.html">READ MORE</a>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4 wow fadeInUp" data-wow-delay="3.3s">
                <img src="assets/media/3-column-info/2.jpg" alt="Img">
                <span></span>
                <h3>SAFE & SECURE DELIVERY</h3>
                <p>Integer congue, elit non semper laoreet sed lectus orci posuh nisl tempor lacus felis ac mauris. Pellentesque in urna. Intege vitae felis vel magna posuere vestibulum.</p>
                <a class="btn btn-default btn-sm" href="11_blog-details.html">READ MORE</a>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4 wow fadeInRight" data-wow-delay="3.3s">
                <img src="assets/media/3-column-info/3.jpg" alt="Img">
                <span></span>
                <h3>SAFE & SECURE DELIVERY</h3>
                <p>Integer congue, elit non semper laoreet sed lectus orci posuh nisl tempor lacus felis ac mauris. Pellentesque in urna. Intege vitae felis vel magna posuere vestibulum.</p>
                <a class="btn btn-default btn-sm" href="11_blog-details.html">READ MORE</a>
            </div>
        </div>
    </div>

    <hr>
    <div class="big-hr color-1 wow zoomInUp" data-wow-delay="0.3s">
        <div class="text-left" style="margin-right:40px;">
            <h2>To  apply for a place, download and complete the application form.</h2>
            <p>Institute of Logistics and Supply Chain Management, 7th Floor Victor House, Kimathi Street, Nairobi</p>
        </div>
        <div><a class="btn btn-success btn-lg" href="#">DOWNLOAD FORM</a></div>
    </div>

    <div class="container-fluid block-content">
        <div class="text-center hgroup wow zoomInUp" data-wow-delay="0.3s">
            <h1>OUR COURSES</h1>
            <h2>We have wide network of offices in all major locations to help you with <br> the services we offer</h2>
        </div>
        <div class="row our-services">
            <div class="col-sm-6 col-md-4 col-lg-4 wow zoomInLeft" data-wow-delay="0.3s">
                <a href="{{route('one')}}">
                    <span><i class="glyph-icon flaticon-boats4"></i></span>
                    <h4>Diploma in Logistics and Transport</h4>
                    <p>Integer congue elit non semper laore lectus orci posuer nisl tempor lacus mauris led ipsum.</p>
                </a>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-4 wow zoomInUp" data-wow-delay="0.3s">
                <a href="11_blog-details.html">
                    <span><i class="glyph-icon flaticon-flying"></i></span>
                    <h4>Professional Diploma in Logistics and Transport</h4>
                    <p>Integer congue elit non semper laore lectus orci posuer nisl tempor lacus mauris led ipsum.</p>
                </a>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-4 wow zoomInRight" data-wow-delay="0.3s">
                <a href="11_blog-details.html">
                    <span><i class="glyph-icon flaticon-garage1"></i></span>
                    <h4>Advanced Diploma in Logistcs and Transport</h4>
                    <p>Integer congue elit non semper laore lectus orci posuer nisl tempor lacus mauris led ipsum.</p>
                </a>
            </div>
{{--            <div class="col-sm-6 col-md-4 col-lg-4 wow zoomInLeft" data-wow-delay="0.3s">--}}
{{--                <a href="11_blog-details.html">--}}
{{--                    <span><i class="glyph-icon flaticon-package7"></i></span>--}}
{{--                    <h4>SEA FREIGHT</h4>--}}
{{--                    <p>Integer congue elit non semper laore lectus orci posuer nisl tempor lacus mauris led ipsum.</p>--}}
{{--                </a>--}}
{{--            </div>--}}
{{--            <div class="col-sm-6 col-md-4 col-lg-4 wow zoomInUp" data-wow-delay="0.3s">--}}
{{--                <a href="11_blog-details.html">--}}
{{--                    <span><i class="glyph-icon flaticon-railway1"></i></span>--}}
{{--                    <h4>SEA FREIGHT</h4>--}}
{{--                    <p>Integer congue elit non semper laore lectus orci posuer nisl tempor lacus mauris led ipsum.</p>--}}
{{--                </a>--}}
{{--            </div>--}}
{{--            <div class="col-sm-6 col-md-4 col-lg-4 wow zoomInRight" data-wow-delay="0.3s">--}}
{{--                <a href="11_blog-details.html">--}}
{{--                    <span><i class="glyph-icon flaticon-traffic-signal"></i></span>--}}
{{--                    <h4>SEA FREIGHT</h4>--}}
{{--                    <p>Integer congue elit non semper laore lectus orci posuer nisl tempor lacus mauris led ipsum.</p>--}}
{{--                </a>--}}
{{--            </div>--}}
        </div>
    </div>

    <div class="fleet-gallery block-content bg-image inner-offset">
        <div class="container-fluid inner-offset">
            <div class="text-center hgroup wow zoomInUp" data-wow-delay="0.3s">
                <h1>THE FLEETS GALLERY</h1>
                <h2>we always use best & fastest fleets</h2>
            </div>
            <div id="fleet-gallery" class="owl-carousel enable-owl-carousel" data-pagination="false" data-navigation="true" data-min450="2" data-min600="2" data-min768="4">
                <div class="wow rotateIn" data-wow-delay="0.3s"><img src="assets/media/fleet-gallery/1.png" alt="Img"></div>
                <div class="wow rotateIn" data-wow-delay="0.3s"><img src="assets/media/fleet-gallery/2.png" alt="Img"></div>
                <div class="wow rotateIn" data-wow-delay="0.3s"><img src="assets/media/fleet-gallery/3.png" alt="Img"></div>
                <div class="wow rotateIn" data-wow-delay="0.3s"><img src="assets/media/fleet-gallery/4.png" alt="Img"></div>
                <div class="wow rotateIn" data-wow-delay="0.3s"><img src="assets/media/fleet-gallery/1.png" alt="Img"></div>
                <div class="wow rotateIn" data-wow-delay="0.3s"><img src="assets/media/fleet-gallery/2.png" alt="Img"></div>
            </div>
        </div>
    </div>

    <div class="container-fluid block-content">
        <div class="row">
            <div class="col-md-6 col-lg-6 wow fadeInLeft" data-wow-delay="0.3s">
                <div class="hgroup">
                    <h1>TRUSTED CLIENTS</h1>
                    <h2>Lorem ipsum dolor sit amet consectetur</h2>
                </div>
                <div id="testimonials" class="owl-carousel enable-owl-carousel" data-single-item="true" data-pagination="false" data-navigation="true" data-auto-play="true">
                    <div>
                        <div class="testimonial-content">
                            <span><i class="fa fa-quote-left"></i></span>
                            <p>Integer congue elit non semper laoreet sed lectus orci posuer nisl tempor se felis ac mauris. Pelentesque inyd urna. Integer vitae felis vel magna posu du vestibulum. Nam rutrum congue diam. Aliquam malesuada maurs etug met Curabitur laoreet convallis nisal pellentesque bibendum.</p>
                        </div>
                        <div class="text-right testimonial-author">
                            <h4>JOHN DEO</h4>
                            <small>Managing Director</small>
                        </div>
                    </div>
                    <div>
                        <div class="testimonial-content">
                            <span><i class="fa fa-quote-left"></i></span>
                            <p>Integer congue elit non semper laoreet sed lectus orci posuer nisl tempor se felis ac mauris. Pelentesque inyd urna. Integer vitae felis vel magna posu du vestibulum. Nam rutrum congue diam. Aliquam malesuada maurs etug met Curabitur laoreet convallis nisal pellentesque bibendum.</p>
                        </div>
                        <div class="text-right testimonial-author">
                            <h4>JOHN DEO</h4>
                            <small>Managing Director</small>
                        </div>
                    </div>
                    <div>
                        <div class="testimonial-content">
                            <span><i class="fa fa-quote-left"></i></span>
                            <p>Integer congue elit non semper laoreet sed lectus orci posuer nisl tempor se felis ac mauris. Pelentesque inyd urna. Integer vitae felis vel magna posu du vestibulum. Nam rutrum congue diam. Aliquam malesuada maurs etug met Curabitur laoreet convallis nisal pellentesque bibendum.</p>
                        </div>
                        <div class="text-right testimonial-author">
                            <h4>JOHN DEO</h4>
                            <small>Managing Director</small>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-6 col-lg-6 wow fadeInRight" data-wow-delay="0.3s">
                <div class="hgroup">
                    <h1>WHY CHOOSE US</h1>
                    <h2>Lorem ipsum dolor sit amet consectetur</h2>
                </div>
                <ul class="why-us">
                    <li>
                        Dui ac hendrerit elementum quam ipsum auctor lorem
                        <p>Integer congue elit non semper laoreet sed lectus orci posuer nisl tempor se felis ac mauris. Pelentesque inyd urna. Integer vitae felis vel magna posu du vestibulum. Nam rutrum congue diam. Aliquam malesuada maurs etug met Curabitur laoreet convallis nisal pellentesque bibendum.</p>
                        <span>+</span>
                    </li>
                    <li>
                        Mauris vel magna a est lobortis volutpat
                        <p>Integer congue elit non semper laoreet sed lectus orci posuer nisl tempor se felis ac mauris. Pelentesque inyd urna. Integer vitae felis vel magna posu du vestibulum. Nam rutrum congue diam. Aliquam malesuada maurs etug met Curabitur laoreet convallis nisal pellentesque bibendum.</p>
                        <span>+</span>
                    </li>
                    <li>
                        Sed bibendum ornare lorem mauris feugiat suspendisse neque
                        <p>Integer congue elit non semper laoreet sed lectus orci posuer nisl tempor se felis ac mauris. Pelentesque inyd urna. Integer vitae felis vel magna posu du vestibulum. Nam rutrum congue diam. Aliquam malesuada maurs etug met Curabitur laoreet convallis nisal pellentesque bibendum.</p>
                        <span>+</span>
                    </li>
                    <li>
                        Nulla scelerisque dul hendrerit elementum quam
                        <p>Integer congue elit non semper laoreet sed lectus orci posuer nisl tempor se felis ac mauris. Pelentesque inyd urna. Integer vitae felis vel magna posu du vestibulum. Nam rutrum congue diam. Aliquam malesuada maurs etug met Curabitur laoreet convallis nisal pellentesque bibendum.</p>
                        <span>+</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <hr>

    <div class="container-fluid block-content percent-blocks" data-waypoint-scroll="true">
        <div class="row stats">
            <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="chart" data-percent="230">
                    <span><i class="fa fa-folder-open"></i></span>
                    <span class="percent"></span>Courses offered
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="chart" data-percent="68">
                    <span><i class="fa fa-users"></i></span>
                    <span class="percent"></span>Students
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="chart" data-percent="147">
                    <span><i class="fa fa-truck"></i></span>
                    <span class="percent"></span>Completed students
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="chart" data-percent="105">
                    <span><i class="fa fa-male"></i></span>
                    <span class="percent"></span>Tutors
                </div>
            </div>
        </div>
    </div>

@endsection
